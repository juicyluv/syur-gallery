package main

import (
	"flag"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/app"
)

var (
	configName = flag.String("config-name", config.DefaultConfigFileName, "server config file name in configs folder")
)

// @title Галерея СЮР REST API
// @version 1.0
// @description Документация по использованию REST API.

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @description Данные нужно вводить в формате `Bearer <AuthToken>`

func main() {
	flag.Parse()

	apl, err := app.New(*configName)

	if err != nil {
		panic(err)
	}

	if err := apl.Start(apl.Service()); err != nil {
		panic(err)
	}
}
