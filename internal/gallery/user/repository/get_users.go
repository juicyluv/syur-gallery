package repository

import (
	"context"
	"fmt"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (r *repo) GetUsers(ctx context.Context, request *domain.GetUsersRequest) (*domain.GetUsersResponse, error) {

	// language=PostgreSQL
	var users = make([]domain.User, 0)
	query := `select id, username, email, created_at, is_verified, is_banned, is_trusted, description, last_login from users `

	args := make([]interface{}, 0)
	argId := 1

	searchQuery := request.Search
	sortOrder := request.SortOrder
	sortField := request.SortField
	page := request.Page
	pageSize := request.PageSize
	offset := 0

	if searchQuery != nil {
		query = query + fmt.Sprintf(` where username ilike $%d or email ilike $%d or full_name=$%d `, argId, argId, argId)
		args = append(args, searchQuery)
		argId++
	}

	if sortField != nil {
		if *sortField == "username" {
			query = query + `order by username`
		} else if *sortField == "email" {
			query = query + `order by email`
		} else if *sortField == "last_login" {
			query = query + `order by last_login`
		} else {
			query = query + `order by id`
		}

	} else {
		query = query + `order by id`
	}

	if sortOrder != nil {
		if *sortOrder == 0 {
			query = query + ` asc`
		} else if *sortOrder == 1 {
			query = query + ` desc`
		}
	}

	if pageSize != nil {
		if *pageSize <= 0 {
			*pageSize = 60
		}
	}

	if page != nil {
		if *page <= 1 {
			*page = 1
		} else {
			offset = *pageSize * (*page - 1)
		}
	}

	query = query + fmt.Sprintf(` limit $%d`, argId)
	args = append(args, pageSize)
	argId++
	query = query + fmt.Sprintf(` offset $%d`, argId)
	args = append(args, offset)
	argId++

	row, err := r.db.Query(ctx, query, args...)

	if err != nil {
		return nil, err
	}

	for row.Next() {
		var user domain.User
		err := row.Scan(
			&user.Id,
			&user.Username,
			&user.Email,
			&user.CreatedAt,
			&user.IsVerified,
			&user.IsBanned,
			&user.IsTrusted,
			&user.Description,
			&user.LastLogin,
		)

		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}
	return &domain.GetUsersResponse{Users: users}, nil
}
