package repository

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (r *repo) UpdateUser(ctx context.Context, input *domain.UpdateUserRequest) error {
	token := ctx.Value("token").(*domain.AuthToken)
	values := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if input.Email != nil {
		values = append(values, fmt.Sprintf("email=$%d", argId))
		args = append(args, *input.Email)
		argId++
	}

	//if input.Password != nil {
	//	var user *domain.User
	//
	//	values = append(values, fmt.Sprintf("password_hash=$%d", argId))
	//	hashedPassword, err := user.GeneratePasswordHash(*input.Password)
	//
	//	if err != nil {
	//		return err
	//	}
	//
	//	args = append(args, hashedPassword)
	//	argId++
	//}

	if input.Username != nil {
		values = append(values, fmt.Sprintf("username=$%d", argId))
		args = append(args, strings.ToLower(*input.Username))
		argId++
	}

	if input.FullName != nil {
		values = append(values, fmt.Sprintf("full_name=$%d", argId))
		args = append(args, *input.FullName)
		argId++
	}

	if input.Description != nil {
		values = append(values, fmt.Sprintf("description=$%d", argId))
		args = append(args, *input.Description)
		argId++
	}

	setQuery := strings.Join(values, ",")

	if argId == 1 {
		return nil
	}

	query := fmt.Sprintf("UPDATE users SET %s WHERE id=$%d", setQuery, argId)
	args = append(args, token.UserId)

	_, err := r.db.Exec(ctx, query, args...)

	if err != nil {
		fmt.Println("err")
		return err
	}

	return nil

}
