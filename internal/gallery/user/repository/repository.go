package repository

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/database"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
)

type UserRepository interface {
	GetUsers(ctx context.Context, request *domain.GetUsersRequest) (*domain.GetUsersResponse, error)
	GetUser(ctx context.Context, request *domain.GetUserRequest) (*domain.GetUserResponse, error)
	DeleteUser(ctx context.Context) (*domain.DeleteUserResponse, error)
	UpdateUser(ctx context.Context, input *domain.UpdateUserRequest) error
}

type repo struct {
	logger *logger.Logger
	db     database.Driver
}

func NewUserRepository(logger *logger.Logger, db database.Driver) UserRepository {
	return &repo{
		logger: logger,
		db:     db,
	}
}
