package repository

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (r *repo) DeleteUser(ctx context.Context) (*domain.DeleteUserResponse, error) {
	token := ctx.Value("token").(*domain.AuthToken)
	var result string
	query := `delete from users where id=$1`
	row := r.db.QueryRow(ctx, query, token.UserId)
	row.Scan(&result)

	return &domain.DeleteUserResponse{}, nil
}
