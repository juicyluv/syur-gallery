package repository

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v5"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

func (r *repo) GetUser(ctx context.Context, request *domain.GetUserRequest) (*domain.GetUserResponse, error) {

	// TODO: получать данные о бане, затем, если пользователь не заблокирован, получать теги
	query := `select email, username, full_name, created_at, last_login, is_banned, is_verified, avatar_path, description from users where id = $1`

	var user domain.User

	row := r.db.QueryRow(ctx, query, request.Id)
	err := row.Scan(
		&user.Email,
		&user.Username,
		&user.FullName,
		&user.CreatedAt,
		&user.LastLogin,
		&user.IsBanned,
		&user.IsVerified,
		&user.AvatarPath,
		&user.Description,
	)
	user.Id = *request.Id

	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, errdomain.ErrObjectNotFound
		}

		return nil, err
	}

	return &domain.GetUserResponse{User: user}, nil
}
