package usecase

import (
	"context"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (u *usecase) GetUser(ctx context.Context, request *domain.GetUserRequest) (*domain.GetUserResponse, error) {

	users, err := u.repo.GetUser(ctx, request)

	if err != nil {
		return nil, err
	}

	return users, nil
}
