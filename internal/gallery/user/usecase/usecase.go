package usecase

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/user/repository"
)

type UserUseCase interface {
	GetUsers(ctx context.Context, request *domain.GetUsersRequest) (*domain.GetUsersResponse, error)
	GetUser(ctx context.Context, request *domain.GetUserRequest) (*domain.GetUserResponse, error)
	DeleteUser(ctx context.Context) (*domain.DeleteUserResponse, error)
	UpdateUser(ctx context.Context, input *domain.UpdateUserRequest) (*domain.GetUserResponse, error)
}

type usecase struct {
	logger *logger.Logger
	repo   repository.UserRepository
}

func NewUserUseCase(logger *logger.Logger, repo repository.UserRepository) UserUseCase {
	return &usecase{
		logger: logger,
		repo:   repo,
	}
}
