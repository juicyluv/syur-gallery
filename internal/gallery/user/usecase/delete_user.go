package usecase

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (u *usecase) DeleteUser(ctx context.Context) (*domain.DeleteUserResponse, error) {

	response, err := u.repo.DeleteUser(ctx)

	if err != nil {
		return response, err
	}

	return response, nil
}
