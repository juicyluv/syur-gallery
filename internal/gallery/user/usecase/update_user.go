package usecase

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (u *usecase) UpdateUser(ctx context.Context, input *domain.UpdateUserRequest) (*domain.GetUserResponse, error) {

	err := u.repo.UpdateUser(ctx, input)

	if err != nil {
		return nil, err
	}

	token := ctx.Value("token").(*domain.AuthToken)

	user, err := u.repo.GetUser(ctx, &domain.GetUserRequest{Id: &token.UserId})

	return user, err
}
