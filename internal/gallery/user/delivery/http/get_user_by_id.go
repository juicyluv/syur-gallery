package http

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

// Users godoc
// @Summary      Позволяет получить пользователя по id.
// @Description  Укажите id пользователя в пути запроса
// @Tags         Users
// @Produce      json
// @Param        userId	path int64  true  "Идентификатор пользователя, которого нужно получить"
// @Success      200  {object}  domain.GetUserResponse
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Router       /api/v1/users/{userId} [get]
func (h *UserHandler) GetUser(w http.ResponseWriter, r *http.Request) {

	userId := chi.URLParam(r, "userId")

	id, err := strconv.ParseInt(userId, 10, 64)

	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, err, nil)
		return
	}

	var input domain.GetUserRequest
	input.Id = &id

	response, err := h.uc.GetUser(r.Context(), &input)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
