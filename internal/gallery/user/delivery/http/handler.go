package http

import (
	"github.com/go-chi/chi"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/auth"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/user/usecase"
)

type UserHandler struct {
	logger *logger.Logger
	uc     usecase.UserUseCase
	cfg    config.Config
}

func NewUserHandler(logger *logger.Logger, uc usecase.UserUseCase, cfg config.Config) *UserHandler {
	return &UserHandler{
		logger: logger,
		uc:     uc,
		cfg:    cfg,
	}
}

func (h *UserHandler) RegisterEndpoints(router chi.Router) {
	router.Get(`/api/v1/users`, h.GetUsers)
	router.Get(`/api/v1/users/{userId}`, h.GetUser)
	router.Delete(`/api/v1/users`, auth.RequireAuth(h.DeleteUser, h.cfg))
	router.Patch(`/api/v1/users`, auth.RequireAuth(h.UpdateUser, h.cfg))
}
