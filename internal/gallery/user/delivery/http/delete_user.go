package http

import (
	"net/http"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
)

// Users godoc
// @Summary      Удаление пользователя
// @Description  Используется для удаления пользователя из базы данных посредством передачи его Id
// @Tags         Users
// @Produce      json
// @Success      200  {object}  domain.DeleteUserResponse
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Security     ApiKeyAuth
// @Router       /api/v1/users [delete]
func (h *UserHandler) DeleteUser(w http.ResponseWriter, r *http.Request) {

	response, err := h.uc.DeleteUser(r.Context())

	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, err, nil)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
