package http

import (
	"net/http"
	"strconv"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

// Users godoc
// @Summary      Позволяет получить список пользователей.
// @Description  Используется для получения списка пользователей.
// @Tags         Users
// @Produce      json
// @Param        page_size     query int     false    "Максимальное возвращаемое количество найденных элементов в списке" default(20) minimum(1)
// @Param        page          query int     false    "Номер возвращаемой страницы" default(1)
// @Param        sort_order    query int     false    "Определяет порядок сортировки: 0 - прямой, 1 - обратный"
// @Param        sort_field    query string  false    "Определяет поле, по которому проводится сортировка (username, email, last_login)"
// @Param        search        query string  false    "Пытается найти совпадения строки с именем или почтой пользователя"
// @Success      200  {object}  domain.GetUsersResponse
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Router       /api/v1/users [get]
func (h *UserHandler) GetUsers(w http.ResponseWriter, r *http.Request) {

	var input domain.GetUsersRequest
	query := r.URL.Query()

	if v := query.Get("search"); v != "" {
		input.Search = &v
	}

	if v := query.Get("sort_field"); v != "" {
		input.SortField = &v
	}

	if v := query.Get("sort_order"); v != "" {
		n, err := strconv.Atoi(v)

		if err == nil {
			input.SortOrder = &n
		}
	}

	if v := query.Get("page_size"); v != "" {
		n, err := strconv.Atoi(v)

		if err == nil {
			input.PageSize = &n
		}
	}

	if v := query.Get("page"); v != "" {
		n, err := strconv.Atoi(v)

		if err == nil {
			input.Page = &n
		}
	}

	response, err := h.uc.GetUsers(r.Context(), &input)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
