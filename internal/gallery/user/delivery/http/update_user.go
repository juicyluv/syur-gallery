package http

import (
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
	"net/http"
)

// Users godoc
// @Summary      Позволяет обновить запись пользователя в базе данных.
// @Description  .
// @Tags         Users
// @Accept       json
// @Produce      json
// @Param        request body domain.UpdateUserRequest true "Информация, которую нужно изменить"
// @Success      200  {object}  domain.UpdateUserResponse
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Security     ApiKeyAuth
// @Router       /api/v1/users [patch]
func (h *UserHandler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	var input domain.UpdateUserRequest

	if err := json.ReadHttpBody(r, &input); err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.InvalidJsonError, nil)
		return
	}

	response, err := h.uc.UpdateUser(r.Context(), &input)

	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, err, nil)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
