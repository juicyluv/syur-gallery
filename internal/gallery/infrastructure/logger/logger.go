package logger

import "go.uber.org/zap"

type Level int

const (
	LevelDebug = iota
)

type Logger struct {
	*zap.Logger
	LoggerLevel Level
}

func NewLogger() (*Logger, error) {
	l, err := zap.NewDevelopment()

	if err != nil {
		return nil, err
	}

	return &Logger{
		Logger: l,
	}, nil
}
