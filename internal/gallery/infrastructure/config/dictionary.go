package config

const (
	DefaultConfigDir      = `configs`
	DefaultConfigFileName = `cfg`

	LayerSeparator = `.`

	AppLayer = `app`

	DeliveryLayer = AppLayer + LayerSeparator + `delivery`

	HttpLayer          = DeliveryLayer + LayerSeparator + `http`
	HttpPort           = HttpLayer + LayerSeparator + `port`
	HttpWriteTimeout   = HttpLayer + LayerSeparator + `writeTimeout`
	HttpReadTimeout    = HttpLayer + LayerSeparator + `readTimeout`
	HttpMaxHeaderBytes = HttpLayer + LayerSeparator + `maxHeaderBytes`

	DatabaseLayer = AppLayer + LayerSeparator + `database`

	DatabaseUser     = DatabaseLayer + LayerSeparator + `user`
	DatabasePassword = DatabaseLayer + LayerSeparator + `password`
	DatabaseHost     = DatabaseLayer + LayerSeparator + `host`
	DatabasePort     = DatabaseLayer + LayerSeparator + `port`
	DatabaseName     = DatabaseLayer + LayerSeparator + `database`
	DatabaseSslMode  = DatabaseLayer + LayerSeparator + `sslmode`

	LoggerLayer = AppLayer + LayerSeparator + `logger`
	LoggerLevel = LoggerLayer + LayerSeparator + `level`

	JWTLayer = AppLayer + LayerSeparator + `jwt`

	SigningKey = JWTLayer + LayerSeparator + `signingKey`
)
