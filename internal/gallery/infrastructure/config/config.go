package config

type Config interface {
	HttpServer
	Database
	Logger
	Auth
}

type Auth interface {
	SigningKey() string
	SetSigningKey(val string)
}

type HttpServer interface {
	HttpPort() string
	SetHttpPort(val string)

	HttpWriteTimeout() int32
	SetHttpWriteTimeout(val int32)

	HttpReadTimeout() int32
	SetHttpReadTimeout(val int32)

	HttpMaxHeaderBytes() int32
	SetHttpMaxHeaderBytes(val int32)
}

type Database interface {
	DatabaseUser() string
	SetDatabaseUser(val string)

	DatabasePassword() string
	SetDatabasePassword(val string)

	DatabaseHost() string
	SetDatabaseHost(val string)

	DatabaseName() string
	SetDatabaseName(val string)

	DatabasePort() string
	SetDatabasePort(val string)

	DatabaseSslMode() string
	SetDatabaseSslMode(val string)

	DatabaseConnString() string
}

type Logger interface {
	LoggerLevel() string
	SetLoggerLevel(val string)
}
