package viper

import "gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"

func (cfg *configuration) SetSigningKey(val string) {
	cfg.viper.Set(config.SigningKey, val)
}

func (cfg *configuration) SigningKey() string {
	return cfg.viper.GetString(config.SigningKey)
}
