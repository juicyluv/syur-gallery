package viper

import (
	"fmt"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"
)

func (cfg *configuration) DatabaseUser() string {
	return cfg.viper.GetString(config.DatabaseUser)
}

func (cfg *configuration) SetDatabaseUser(val string) {
	cfg.viper.Set(config.DatabaseUser, val)
}

func (cfg *configuration) DatabasePassword() string {
	return cfg.viper.GetString(config.DatabasePassword)
}

func (cfg *configuration) SetDatabasePassword(val string) {
	cfg.viper.Set(config.DatabasePassword, val)
}

func (cfg *configuration) DatabaseHost() string {
	return cfg.viper.GetString(config.DatabaseHost)
}

func (cfg *configuration) SetDatabaseHost(val string) {
	cfg.viper.Set(config.DatabaseHost, val)
}

func (cfg *configuration) DatabaseName() string {
	return cfg.viper.GetString(config.DatabaseName)
}

func (cfg *configuration) SetDatabaseName(val string) {
	cfg.viper.Set(config.DatabaseName, val)
}

func (cfg *configuration) DatabasePort() string {
	return cfg.viper.GetString(config.DatabasePort)
}

func (cfg *configuration) SetDatabasePort(val string) {
	cfg.viper.Set(config.DatabasePort, val)
}

func (cfg *configuration) DatabaseSslMode() string {
	return cfg.viper.GetString(config.DatabaseSslMode)
}

func (cfg *configuration) SetDatabaseSslMode(val string) {
	cfg.viper.Set(config.DatabaseSslMode, val)
}

func (cfg *configuration) DatabaseConnString() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s",
		cfg.DatabaseUser(),
		cfg.DatabasePassword(),
		cfg.DatabaseHost(),
		cfg.DatabasePort(),
		cfg.DatabaseName(),
		cfg.DatabaseSslMode(),
	)
}
