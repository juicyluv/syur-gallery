package viper

import (
	"github.com/spf13/viper"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"
)

type configuration struct {
	viper *viper.Viper
}

func NewConfig(fileName string) (config.Config, error) {
	cfg := &configuration{
		viper: viper.NewWithOptions(viper.KeyDelimiter(config.LayerSeparator)),
	}

	cfg.viper.AddConfigPath(config.DefaultConfigDir)
	cfg.viper.SetConfigName(fileName)
	err := cfg.viper.ReadInConfig()

	if err != nil {
		return nil, err
	}

	return cfg, nil
}
