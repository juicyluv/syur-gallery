package viper

import "gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"

func (cfg *configuration) LoggerLevel() string {
	return cfg.viper.GetString(config.LoggerLevel)
}

func (cfg *configuration) SetLoggerLevel(val string) {
	cfg.viper.Set(config.LoggerLevel, val)
}
