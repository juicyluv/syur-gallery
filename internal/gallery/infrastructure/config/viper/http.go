package viper

import "gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"

func (cfg *configuration) HttpPort() string {
	return cfg.viper.GetString(config.HttpPort)
}

func (cfg *configuration) SetHttpPort(val string) {
	cfg.viper.Set(config.HttpPort, val)
}
func (cfg *configuration) HttpWriteTimeout() int32 {
	return cfg.viper.GetInt32(config.HttpWriteTimeout)
}

func (cfg *configuration) SetHttpWriteTimeout(val int32) {
	cfg.viper.Set(config.HttpWriteTimeout, val)
}

func (cfg *configuration) HttpReadTimeout() int32 {
	return cfg.viper.GetInt32(config.HttpReadTimeout)
}

func (cfg *configuration) SetHttpReadTimeout(val int32) {
	cfg.viper.Set(config.HttpReadTimeout, val)
}

func (cfg *configuration) HttpMaxHeaderBytes() int32 {
	return cfg.viper.GetInt32(config.HttpMaxHeaderBytes)
}

func (cfg *configuration) SetHttpMaxHeaderBytes(val int32) {
	cfg.viper.Set(config.HttpMaxHeaderBytes, val)
}
