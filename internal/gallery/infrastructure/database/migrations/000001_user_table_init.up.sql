create table users
(
    id bigserial not null unique,
    email text not null unique,
    password_hash text not null,
    username text not null unique,
    full_name text,
    group_name text,
    created_at timestamp not null default now(),
    last_login timestamp,
    avatar_path text,
    description text,
    is_banned boolean not null default false,
    is_verified boolean not null default false,
    is_trusted boolean not null default false
);