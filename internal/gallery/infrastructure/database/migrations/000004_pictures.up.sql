create table pictures
(
    id bigserial not null unique,
    title text not null,
    author bigint not null references users(id),
    description text,
    created_at timestamp not null default now(),
    status text default 'NOT FOR SALE',
    price money,
    img_path text not null unique
);