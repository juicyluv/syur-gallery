alter table users
    drop column group_id;

alter table users
    add column group_name text;

drop table groups;