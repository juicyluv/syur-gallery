-- noinspection SqlResolve
alter table users
    drop column group_name;

create table groups
(
    id bigserial not null unique,
    name text not null unique
);

alter table users
    add column group_id bigint references groups(id);