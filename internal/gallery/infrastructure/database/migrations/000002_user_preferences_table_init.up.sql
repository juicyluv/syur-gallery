create table user_preferences
(
    user_id bigint not null unique references users(id),

    comment_notification boolean not null default true,
    picture_rating_notification boolean not null default true,
    picture_subscription_notification boolean not null default true,
    user_subscription_notification boolean not null default true,
    purchase_request_notification boolean not null default true,

    show_subscriptions boolean not null default true,
    show_favourites boolean not null default true
);