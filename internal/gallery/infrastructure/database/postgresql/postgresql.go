package postgresql

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/database"
)

type postgresql struct {
	conn *pgx.Conn
}

// NewPostgreSQLConnection пытается установить подключение с базой данных
// с помощью строки подключения DSN.
// При успешном подключении пингует бд, в ином случае возвращает ошибку.
func NewPostgreSQLConnection(connString string) (database.Driver, error) {
	cfg, err := pgx.ParseConfig(connString)

	if err != nil {
		return nil, fmt.Errorf("failed to parse conn string: %w", err)
	}

	conn, err := pgx.ConnectConfig(context.Background(), cfg)

	if err != nil {
		return nil, fmt.Errorf("cannot connect to postgres: %w", err)
	}

	if err = conn.Ping(context.Background()); err != nil {
		return nil, fmt.Errorf("cannot ping database: %w", err)
	}

	return &postgresql{conn}, nil
}

func (p *postgresql) Query(ctx context.Context, sql string, args ...any) (database.Rows, error) {
	return p.conn.Query(ctx, sql, args...)
}

func (p *postgresql) QueryRow(ctx context.Context, sql string, args ...any) database.Row {
	return p.conn.QueryRow(ctx, sql, args...)
}
func (p *postgresql) Exec(ctx context.Context, sql string, args ...any) (int64, error) {
	commandTag, err := p.conn.Exec(ctx, sql, args...)
	RowsAffected := commandTag.RowsAffected()
	return RowsAffected, err
}

func (p *postgresql) Close(ctx context.Context) error {
	return p.conn.Close(ctx)
}

func (p *postgresql) CheckIfUserExist(ctx context.Context) error {

	token := ctx.Value("token").(*domain.AuthToken)
	query := `select exists(select * from users where email=$1)`
	var userExist bool

	row := p.QueryRow(ctx, query, token.Email)

	row.Scan(
		&userExist,
	)
	if !userExist {
		return errdomain.UserDoesNotExistError
	}
	return nil
}
