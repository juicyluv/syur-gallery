package domain

import "github.com/golang-jwt/jwt/v4"

type SignInRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type SignUpRequest struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type AuthRequest struct {
}

type CheckUserRequest struct {
	Email    string `json:"email"`
	Username string `json:"username"`
}

type SignInResponse struct {
	Token string `json:"token,omitempty"`
	User  User   `json:"user"`
}

type SignUpResponse struct {
	Token string `json:"token,omitempty"`
	User  User   `json:"user"`
}

type AuthResponse struct {
	Token string `json:"token,omitempty"`
	User  User   `json:"user"`
}

type AuthToken struct {
	jwt.RegisteredClaims
	UserId int64 `json:"id"`
	// Role         string `json:"role"`
	Email      string  `json:"email"`
	Username   string  `json:"username"`
	FullName   *string `json:"fullName,omitempty"`
	AvatarPath *string `json:"avatarPath,omitempty"`
}
