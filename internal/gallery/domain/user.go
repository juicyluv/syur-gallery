package domain

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

const (
	cost = 10
)

type User struct {
	Id          int64      `json:"id"`
	Email       string     `json:"email"`
	Password    string     `json:"-"`
	Username    string     `json:"username"`
	CreatedAt   time.Time  `json:"createdAt"`
	IsBanned    bool       `json:"isBanned"`
	IsVerified  bool       `json:"isVerified"`
	IsTrusted   bool       `json:"isTrusted"`
	GroupId     *int64     `json:"groupId,omitempty"`
	FullName    *string    `json:"fullName,omitempty"`
	AvatarPath  *string    `json:"avatarPath,omitempty"`
	Description *string    `json:"description,omitempty"`
	LastLogin   *time.Time `json:"lastLogin,omitempty"`
}

type GetUserRequest struct {
	Id *int64
}

type GetUserResponse struct {
	User User `json:"user"`
}

type GetUsersRequest struct {
	Search    *string
	SortField *string
	SortOrder *int
	PageSize  *int
	Page      *int
}

type UpdateUserRequest struct {
	Email       *string `json:"email"`
	Username    *string `json:"username"`
	FullName    *string `json:"fullName,omitempty"`
	Description *string `json:"description,omitempty"`
}

type UpdateUserResponse struct {
	User User `json:"user"`
}

type GetUsersResponse struct {
	Users []User `json:"users"`
}

type CreateUserResponse struct{}

type CreateUserRequest struct{}

func (user *User) GeneratePasswordHash(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	return string(bytes), err
}

func (user *User) ComparePasswords(password string, hashedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	return err
}

type DeleteUserResponse struct{}
