package domain

import "time"

type Picture struct {
	Id          int64     `json:"id"`
	Author      int64     `json:"author"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"createdAt"`
	Status      string    `json:"status"`
	Price       float64   `json:"price"`
	ImgPath     string    `json:"imgPath"`
}

type PictureBrief struct {
	Id      int64  `json:"id"`
	Author  int64  `json:"author"`
	Title   string `json:"title"`
	ImgPath string `json:"imgPath"`
}

type AddPictureRequest struct {
	Title       string
	Description string
	Status      string
	Price       string
	ImgPath     string
}

type AddPictureResponse struct {
	Id int64 `json:"id"`
}

type GetPictureRequest struct {
	Id *int64 `json:"id"`
}

type GetPictureResponse struct {
	Picture Picture `json:"picture"`
}

type GetPicturesRequest struct {
	Search    *string
	SortField *string
	SortOrder *int
	PageSize  *int
	Page      *int
}

type GetPicturesResponse struct {
	Pictures []PictureBrief `json:"pictures"`
}

type UpdatePictureRequest struct {
	Id          int64
	Title       *string
	Description *string
	Status      *string
	Price       *string
	ImgPath     *string
}

type UpdatePictureResponse struct {
	Picture Picture `json:"picture"`
}
