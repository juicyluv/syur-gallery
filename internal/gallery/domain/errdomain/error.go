package errdomain

type ErrorResponse struct {
	Message string `json:"message"`
}

func (e *ErrorResponse) Error() string {
	return e.Message
}

var (
	InvalidJsonError = &ErrorResponse{
		Message: "Invalid JSON provided",
	}
	FileIsTooBigError = &ErrorResponse{
		Message: "File is too big",
	}
	FailedToReadFileError = &ErrorResponse{
		Message: "Failed to read file.",
	}
	FailedToCreateFileError = &ErrorResponse{
		Message: "Failed to create file.",
	}
	FailedToWriteFileError = &ErrorResponse{
		Message: "Failed to write file.",
	}
	UserDoesNotExistError = &ErrorResponse{
		Message: "User does not exist",
	}
	InvalidIdError = &ErrorResponse{
		Message: "Invalid id provided",
	}
)
