package errdomain

var (
	InvalidPictureTitleError = &ErrorResponse{
		Message: "Invalid picture title.",
	}

	PictureDoesNotExist = &ErrorResponse{
		Message: "The picture you request seems to not exist.",
	}
)
