package gallery

import (
	"github.com/kardianos/service"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
)

type Application interface {
	service.Interface

	Service() service.Service

	GetLogger() *logger.Logger

	SetLogger(logger *logger.Logger)

	// GetConfig() config.Interface

	// SetConfig(cfg config.Interface)
}
