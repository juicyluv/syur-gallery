package repository

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

func (r *repo) UpdatePicture(ctx context.Context, picture *domain.UpdatePictureRequest) (*domain.UpdatePictureResponse, error) {

	err := r.db.CheckIfUserExist(ctx)

	values := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if picture.Title != nil {
		values = append(values, fmt.Sprintf("title=$%d", argId))
		args = append(args, *&picture.Title)
		argId++
	}

	if picture.Description != nil {
		values = append(values, fmt.Sprintf("description=$%d", argId))
		args = append(args, *picture.Description)
		argId++
	}

	if picture.Status != nil {
		values = append(values, fmt.Sprintf("status=$%d", argId))
		args = append(args, *picture.Status)
		argId++
	}

	if picture.ImgPath != nil {
		values = append(values, fmt.Sprintf("img_path=$%d", argId))
		args = append(args, *picture.ImgPath)
		argId++
	}

	if picture.Price != nil {
		values = append(values, fmt.Sprintf("price=$%d", argId))
		args = append(args, *picture.Price)
		argId++
	}

	setQuery := strings.Join(values, ",")

	query := fmt.Sprintf("UPDATE pictures SET %s WHERE id=$%d RETURNING id, author, title, description, created_at, status, price::numeric, img_path", setQuery, argId)
	args = append(args, picture.Id)

	var updated_picture domain.Picture
	err = r.db.QueryRow(ctx, query, args...).Scan(
		&updated_picture.Id,
		&updated_picture.Author,
		&updated_picture.Title,
		&updated_picture.Description,
		&updated_picture.CreatedAt,
		&updated_picture.Status,
		&updated_picture.Price,
		&updated_picture.ImgPath,
	)

	if err != nil {
		return nil, &errdomain.ErrorResponse{Message: err.Error()}
	}

	return &domain.UpdatePictureResponse{Picture: updated_picture}, err
}
