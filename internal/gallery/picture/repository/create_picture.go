package repository

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

func (r *repo) CreatePicture(ctx context.Context, picture *domain.AddPictureRequest) (*domain.AddPictureResponse, error) {

	token := ctx.Value("token").(*domain.AuthToken)
	err := r.db.CheckIfUserExist(ctx)

	if err != nil {
		return nil, errdomain.UserDoesNotExistError
	}

	query := `INSERT INTO pictures(author, title, description, price, status, img_path) 
	VALUES($1, $2, $3, $4, $5, $6) RETURNING id`

	var Picture domain.Picture

	err = r.db.QueryRow(
		ctx,
		query,
		token.UserId,
		picture.Title,
		picture.Description,
		picture.Price,
		picture.Status,
		picture.ImgPath,
	).Scan(&Picture.Id)

	return &domain.AddPictureResponse{Id: Picture.Id}, err
}
