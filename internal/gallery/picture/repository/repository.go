package repository

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/database"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
)

type PictureRepository interface {
	CreatePicture(ctx context.Context, user *domain.AddPictureRequest) (*domain.AddPictureResponse, error)
	GetPicture(ctx context.Context, request *domain.GetPictureRequest) (*domain.GetPictureResponse, error)
	GetPictures(ctx context.Context, request *domain.GetPicturesRequest) (*domain.GetPicturesResponse, error)
	UpdatePicture(ctx context.Context, user *domain.UpdatePictureRequest) (*domain.UpdatePictureResponse, error)
}

type repo struct {
	logger *logger.Logger
	db     database.Driver
}

func NewPictureRepository(logger *logger.Logger, db database.Driver) PictureRepository {
	return &repo{
		logger: logger,
		db:     db,
	}
}
