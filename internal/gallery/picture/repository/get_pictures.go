package repository

import (
	"context"
	"fmt"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (r *repo) GetPictures(ctx context.Context, request *domain.GetPicturesRequest) (*domain.GetPicturesResponse, error) {

	// language=PostgreSQL
	var pictures = make([]domain.PictureBrief, 0)
	query := `select id, title, author, img_path from pictures `

	args := make([]interface{}, 0)
	argId := 1

	searchQuery := request.Search
	sortOrder := request.SortOrder
	sortField := request.SortField
	page := request.Page
	pageSize := request.PageSize
	offset := 0

	if searchQuery != nil {
		query = query + fmt.Sprintf(` where title ilike $%d or description ilike $%d `, argId, argId)
		args = append(args, searchQuery)
		argId++
	}

	if sortField != nil {
		query = query + fmt.Sprintf(`order by %s `, *sortField)

		if sortOrder != nil {
			if *sortOrder == 0 {
				query = query + `asc `
			} else if *sortOrder == 1 {
				query = query + `desc `
			}
		}
	}

	if pageSize != nil {
		if *pageSize <= 0 {
			*pageSize = 60
		}
	}

	if page != nil {
		if *page <= 1 {
			*page = 1
		} else {
			offset = *pageSize * (*page - 1)
		}
	}

	query = query + fmt.Sprintf(`limit $%d `, argId)
	args = append(args, pageSize)
	argId++
	query = query + fmt.Sprintf(`offset $%d `, argId)
	args = append(args, offset)
	argId++

	row, err := r.db.Query(ctx, query, args...)

	if err != nil {
		return nil, err
	}

	for row.Next() {
		var picture domain.PictureBrief
		err := row.Scan(
			&picture.Id,
			&picture.Title,
			&picture.Author,
			&picture.ImgPath,
		)

		if err != nil {
			return nil, err
		}

		pictures = append(pictures, picture)
	}
	return &domain.GetPicturesResponse{Pictures: pictures}, nil
}
