package repository

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

func (r *repo) GetPicture(ctx context.Context, request *domain.GetPictureRequest) (*domain.GetPictureResponse, error) {

	query := `select author, title, description, created_at, status, price::numeric, img_path from pictures where id = $1`

	var picture domain.Picture

	row := r.db.QueryRow(ctx, query, request.Id)
	err := row.Scan(
		&picture.Author,
		&picture.Title,
		&picture.Description,
		&picture.CreatedAt,
		&picture.Status,
		&picture.Price,
		&picture.ImgPath,
	)
	picture.Id = *request.Id

	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, errdomain.ErrObjectNotFound
		}

		return nil, err
	}

	return &domain.GetPictureResponse{Picture: picture}, nil
}
