package usecase

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (u *usecase) GetPictures(ctx context.Context, request *domain.GetPicturesRequest) (*domain.GetPicturesResponse, error) {

	pictures, err := u.repo.GetPictures(ctx, request)

	if err != nil {
		return nil, err
	}

	return pictures, nil
}
