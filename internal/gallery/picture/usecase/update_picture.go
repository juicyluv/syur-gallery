package usecase

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

func (u *usecase) UpdatePicture(ctx context.Context, request *domain.UpdatePictureRequest) (*domain.UpdatePictureResponse, error) {
	if *request.Title == "" {
		return nil, errdomain.InvalidPictureTitleError
	}

	response, err := u.repo.UpdatePicture(ctx, request)

	if err != nil {
		return nil, err
	}
	return response, nil
}
