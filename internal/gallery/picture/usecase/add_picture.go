package usecase

import (
	"context"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

func (u *usecase) AddPicture(ctx context.Context, request *domain.AddPictureRequest) (*domain.AddPictureResponse, error) {
	if request.Title == "" {
		return nil, errdomain.InvalidPictureTitleError
	}

	response, err := u.repo.CreatePicture(ctx, request)

	if err != nil {
		return nil, err
	}
	return response, nil
}
