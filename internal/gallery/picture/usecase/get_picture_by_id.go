package usecase

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (u *usecase) GetPicture(ctx context.Context, request *domain.GetPictureRequest) (*domain.GetPictureResponse, error) {

	users, err := u.repo.GetPicture(ctx, request)

	if err != nil {
		return nil, err
	}

	return users, nil
}
