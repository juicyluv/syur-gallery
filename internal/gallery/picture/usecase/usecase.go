package usecase

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/picture/repository"
)

type PicturesUseCase interface {
	AddPicture(ctx context.Context, request *domain.AddPictureRequest) (*domain.AddPictureResponse, error)
	GetPicture(ctx context.Context, request *domain.GetPictureRequest) (*domain.GetPictureResponse, error)
	GetPictures(ctx context.Context, request *domain.GetPicturesRequest) (*domain.GetPicturesResponse, error)
	UpdatePicture(ctx context.Context, request *domain.UpdatePictureRequest) (*domain.UpdatePictureResponse, error)
}

type usecase struct {
	logger *logger.Logger
	repo   repository.PictureRepository
}

func NewPictureUseCase(logger *logger.Logger, repo repository.PictureRepository) PicturesUseCase {
	return &usecase{
		logger: logger,
		repo:   repo,
	}
}
