package http

import (
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

// @Summary      Позволяет пользователю добавить картину.
// @Description  Для добавления картины обязательны поля `title`, `image`.
// @Tags         Picture
// @Accept       multipart/form-data
// @Produce      json
// @Param        title formData string true "Название"
// @Param        description formData string false "Описание"
// @Param        status formData string false "Статус (FOR SALE | NOT FOR SALE | SOLD | HIDDEN)"
// @Param        price formData string false "Цена (1250,40)"
// @Param        image formData file true "Изображение картины (не больше 10Мб)"
// @Success      200  {object}	domain.AddPictureResponse
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Security     ApiKeyAuth
// @Router       /api/v1/pictures [post]
func (h *PictureHandler) AddPicture(w http.ResponseWriter, r *http.Request) {
	var input domain.AddPictureRequest

	err := r.ParseMultipartForm(10 << 20)
	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FileIsTooBigError, nil)
		return
	}

	file, handler, err := r.FormFile("image")
	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FailedToReadFileError, nil)
		return
	}
	defer file.Close()

	uuid := uuid.New().String()

	fileBytes, err := io.ReadAll(file)
	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FailedToReadFileError, nil)
		return
	}

	filetype := http.DetectContentType(fileBytes)
	if !strings.HasPrefix(filetype, "image/") {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FailedToReadFileError, nil)
		return
	}

	newFilePath := filepath.Join("public", "img", uuid+filepath.Ext(handler.Filename))

	input.Title = r.FormValue("title")
	input.Description = r.FormValue("description")
	input.Price = r.FormValue("price")
	input.Status = r.FormValue("status")
	input.ImgPath = uuid + filepath.Ext(handler.Filename)

	response, err := h.uc.AddPicture(r.Context(), &input)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	newFile, err := os.Create(newFilePath)
	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FailedToCreateFileError, nil)
		return
	}
	defer newFile.Close()

	_, err = newFile.Write(fileBytes)
	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FailedToWriteFileError, nil)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
