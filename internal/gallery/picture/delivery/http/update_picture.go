package http

import (
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

// @Summary      Обновление данных картины
// @Description  Позволяет пользователю изменить название, описание и цену картины.
// @Tags         Picture
// @Accept       multipart/form-data
// @Produce      json
// @Param        pictureId	path int64  true  "Идентификатор каqртины, которую надо обновить"
// @Param        title formData string false "Название"
// @Param        description formData string false "Описание"
// @Param        status formData string false "Статус (FOR SALE | NOT FOR SALE | SOLD | HIDDEN)"
// @Param        price formData string false "Цена (1250,40)"
// @Param        image formData file false "Изображение картины (не больше 10Мб)"
// @Success      200  {object}	domain.Picture
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Security     ApiKeyAuth
// @Router       /api/v1/pictures/{pictureId} [patch]
func (h *PictureHandler) UpdatePicture(w http.ResponseWriter, r *http.Request) {
	var input domain.UpdatePictureRequest

	err := r.ParseMultipartForm(10 << 20)
	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FileIsTooBigError, nil)
		return
	}

	file, handler, err := r.FormFile("image")

	if err == nil { // Если файл был отправлен
		uuid := uuid.New().String()
		fileBytes, err := io.ReadAll(file)

		if err != nil {
			json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FailedToReadFileError, nil)
			return
		}

		filetype := http.DetectContentType(fileBytes)
		if !strings.HasPrefix(filetype, "image/") {
			json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FailedToReadFileError, nil)
			return
		}
		newFilePath := filepath.Join("public", "img", uuid+filepath.Ext(handler.Filename))

		newFile, err := os.Create(newFilePath)
		if err != nil {
			json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FailedToCreateFileError, nil)
			return
		}
		defer newFile.Close()

		_, err = newFile.Write(fileBytes)
		if err != nil {
			json.SendHttpResponse(w, http.StatusBadRequest, errdomain.FailedToWriteFileError, nil)
			return
		}
		imgPath := uuid + filepath.Ext(handler.Filename)

		input.ImgPath = &imgPath
		defer file.Close()
	}

	pictureId := chi.URLParam(r, "pictureId")
	id, err := strconv.ParseInt(pictureId, 10, 64)

	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.PictureDoesNotExist, nil)
		return
	}

	title := r.FormValue("title")
	description := r.FormValue("description")
	price := r.FormValue("price")
	status := r.FormValue("status")

	input.Id = id
	input.Title = &title
	input.Description = &description
	input.Price = &price
	input.Status = &status

	response, err := h.uc.UpdatePicture(r.Context(), &input)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
