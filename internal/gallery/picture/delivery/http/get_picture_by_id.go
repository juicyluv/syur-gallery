package http

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

// @Summary      Позволяет пользователю получить инофрмацию о картине.
// @Description  Чтобы получить информацию, нужно знать id картины. По запросу можно получить информацию об авторе, названии, описании, статусе и цене картины. А также момент времени, когда она была залита на сервис.
// @Tags         Picture
// @Accept       json
// @Produce      json
// @Param        pictureId	path int64  true  "Идентификатор картины, которую нужно получить"
// @Success      200  {object}	domain.GetPictureResponse
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Router       /api/v1/pictures/{pictureId} [get]
func (h *PictureHandler) GetPicture(w http.ResponseWriter, r *http.Request) {
	pictureId := chi.URLParam(r, "pictureId")

	id, err := strconv.ParseInt(pictureId, 10, 64)

	if err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.InvalidIdError, nil)
		return
	}

	var input domain.GetPictureRequest
	input.Id = &id

	response, err := h.uc.GetPicture(r.Context(), &input)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
