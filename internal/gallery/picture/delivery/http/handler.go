package http

import (
	"github.com/go-chi/chi"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/auth"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/picture/usecase"
)

type PictureHandler struct {
	logger *logger.Logger
	uc     usecase.PicturesUseCase
	cfg    config.Config
}

func NewPictureHandler(logger *logger.Logger, uc usecase.PicturesUseCase, cfg config.Config) *PictureHandler {
	return &PictureHandler{
		logger: logger,
		uc:     uc,
		cfg:    cfg,
	}
}

func (h *PictureHandler) RegisterEndpoints(router chi.Router) {
	router.Get(`/api/v1/pictures`, h.GetPictures)
	router.Post(`/api/v1/pictures`, auth.RequireAuth(h.AddPicture, h.cfg))
	router.Get(`/api/v1/pictures/{pictureId}`, h.GetPicture)
	router.Patch(`/api/v1/pictures/{pictureId}`, auth.RequireAuth(h.UpdatePicture, h.cfg))
}
