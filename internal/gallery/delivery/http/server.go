package http

import (
	"net/http"
	"path/filepath"
	"strings"
	"time"

	"github.com/go-chi/chi"
	httpSwagger "github.com/swaggo/http-swagger"
	authHandler "gitlab.com/juicyluv/syur-gallery/internal/gallery/auth/delivery/http"
	authRepository "gitlab.com/juicyluv/syur-gallery/internal/gallery/auth/repository"
	authUseCase "gitlab.com/juicyluv/syur-gallery/internal/gallery/auth/usecase"

	userHandler "gitlab.com/juicyluv/syur-gallery/internal/gallery/user/delivery/http"
	userRepository "gitlab.com/juicyluv/syur-gallery/internal/gallery/user/repository"
	userUseCase "gitlab.com/juicyluv/syur-gallery/internal/gallery/user/usecase"

	pictureHandler "gitlab.com/juicyluv/syur-gallery/internal/gallery/picture/delivery/http"
	pictureRepository "gitlab.com/juicyluv/syur-gallery/internal/gallery/picture/repository"
	pictureUseCase "gitlab.com/juicyluv/syur-gallery/internal/gallery/picture/usecase"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/database"
	"go.uber.org/zap"

	_ "gitlab.com/juicyluv/syur-gallery/docs"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
)

type Server interface {
	Serve() error
}

type server struct {
	httpServer *http.Server
	logger     *logger.Logger
	cfg        config.Config
	db         database.Driver
}

func NewServer(l *logger.Logger, cfg config.Config, db database.Driver) Server {
	s := &server{
		logger: l,
		cfg:    cfg,
		db:     db,
	}

	s.initializeModules()

	return s
}

func (s *server) Serve() error {
	s.logger.Info("starting http server", zap.String("address", s.cfg.HttpPort()))
	return s.httpServer.ListenAndServe()
}

func (s *server) initializeModules() {
	authRepository := authRepository.NewAuthRepository(s.logger, s.db)
	authUseCase := authUseCase.NewAuthUseCase(s.logger, authRepository, s.cfg)
	authHandler := authHandler.NewAuthHandler(s.logger, authUseCase, s.cfg)

	userRepository := userRepository.NewUserRepository(s.logger, s.db)
	userUseCase := userUseCase.NewUserUseCase(s.logger, userRepository)
	userHandler := userHandler.NewUserHandler(s.logger, userUseCase, s.cfg)

	pictureRepository := pictureRepository.NewPictureRepository(s.logger, s.db)
	pictureUseCase := pictureUseCase.NewPictureUseCase(s.logger, pictureRepository)
	pictureHandler := pictureHandler.NewPictureHandler(s.logger, pictureUseCase, s.cfg)

	handlers := []EndpointRegistrar{
		authHandler,
		userHandler,
		pictureHandler,
	}

	router := chi.NewRouter()

	for _, h := range handlers {
		h.RegisterEndpoints(router)
	}

	router.Get("/api/v1/docs/*", httpSwagger.Handler())

	filesDir := http.Dir(filepath.Join("public"))
	FileServer(router, "/api/v1/files", filesDir)

	s.httpServer = &http.Server{
		Addr:           ":" + s.cfg.HttpPort(),
		Handler:        router,
		ReadTimeout:    time.Duration(s.cfg.HttpReadTimeout()) * time.Second,
		WriteTimeout:   time.Duration(s.cfg.HttpWriteTimeout()) * time.Second,
		MaxHeaderBytes: int(s.cfg.HttpMaxHeaderBytes()) << 20,
	}
}

func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit any URL parameters.")
	}

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}
