package json

import (
	"encoding/json"
	"net/http"
)

// ReadHttpBody парсит JSON данные в переданную структуру.
func ReadHttpBody(r *http.Request, dest any) error {
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()

	if err := decoder.Decode(dest); err != nil {
		return err
	}

	return nil
}

// SendHttpResponse маршалит указанные данные, добавляет переданные заголовки и возвращает ответ с указанным статус кодом.
// Аргумент headers можно не указывать(передавать nil), если нет дополнительных заголовков ответа.
func SendHttpResponse(w http.ResponseWriter, httpStatus int, data any, headers http.Header) {
	object, err := json.Marshal(data)

	if err != nil {
		// TODO: log error
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for key, value := range headers {
		w.Header()[key] = value
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatus)
	_, err = w.Write(object)

	if err != nil {
		// TODO: log error
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
