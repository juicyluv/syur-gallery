package http

import (
	"github.com/go-chi/chi"
)

type EndpointRegistrar interface {
	RegisterEndpoints(router chi.Router)
}
