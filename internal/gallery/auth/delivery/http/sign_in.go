package http

import (
	"errors"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
	"net/http"
)

// SignIn godoc
// @Summary      Позволяет пользователю войти в свой аккаунт.
// @Description  В качестве логина пользователь может использовать `email` или `username` и пароль.
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        request body domain.SignInRequest true "Запрос"
// @Success      200  {object}  domain.SignInResponse
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Router       /api/v1/auth/sign-in [post]
func (h *AuthHandler) SignIn(w http.ResponseWriter, r *http.Request) {
	var input domain.SignInRequest

	if err := json.ReadHttpBody(r, &input); err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.InvalidJsonError, nil)
		return
	}

	response, err := h.uc.SignIn(r.Context(), &input)

	if err != nil {
		if errors.Is(err, errdomain.InvalidCredentialsError) {
			json.SendHttpResponse(w, http.StatusBadRequest, err, nil)
			return
		}

		if errors.Is(err, errdomain.WrongPasswordError) {
			json.SendHttpResponse(w, http.StatusBadRequest, err, nil)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
