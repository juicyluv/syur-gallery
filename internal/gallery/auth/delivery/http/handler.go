package http

import (
	"github.com/go-chi/chi"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/auth"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/auth/usecase"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
)

type AuthHandler struct {
	logger *logger.Logger
	uc     usecase.AuthUseCase
	cfg    config.Config
}

func NewAuthHandler(logger *logger.Logger, uc usecase.AuthUseCase, cfg config.Config) *AuthHandler {
	return &AuthHandler{
		logger: logger,
		uc:     uc,
		cfg:    cfg,
	}
}

func (h *AuthHandler) RegisterEndpoints(router chi.Router) {
	router.Post(`/api/v1/auth/sign-in`, h.SignIn)
	router.Post(`/api/v1/auth/sign-up`, h.SignUp)
	router.Get(`/api/v1/auth`, auth.RequireAuth(h.Auth, h.cfg))
}
