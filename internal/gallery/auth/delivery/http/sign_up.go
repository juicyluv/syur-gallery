package http

import (
	"errors"
	"net/http"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

// @Summary      Позволяет пользователю создать новый аккаунт
// @Description  Для регистрации требуется `email`, `username` и пароль.
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        request body domain.SignUpRequest true "Request"
// @Success      200  {object}  domain.SignUpResponse
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Router       /api/v1/auth/sign-up [post]
func (h *AuthHandler) SignUp(w http.ResponseWriter, r *http.Request) {
	var input domain.SignUpRequest

	if err := json.ReadHttpBody(r, &input); err != nil {
		json.SendHttpResponse(w, http.StatusBadRequest, errdomain.InvalidJsonError, nil)
		return
	}

	response, err := h.uc.SignUp(r.Context(), &input)

	if err != nil {
		if errors.Is(err, errdomain.UsernameAlreadyTakenError) || errors.Is(err, errdomain.EmailAlreadyTakenError) {
			json.SendHttpResponse(w, http.StatusForbidden, err, nil)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
