package http

import (
	"errors"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
	"net/http"
)

// Auth godoc
// @Summary      Позволяет пользователю автоматически войти в аккаунт, если у него имеется JWT.
// @Description  Для обновления токена, нужно передать старый токен.
// @Tags         Auth
// @Produce      json
// @Success      200  {object}  domain.AuthResponse
// @Failure      400  {object}  errdomain.ErrorResponse
// @Failure      500  {object}  errdomain.ErrorResponse
// @Security     ApiKeyAuth
// @Router       /api/v1/auth [get]
func (h *AuthHandler) Auth(w http.ResponseWriter, r *http.Request) {
	response, err := h.uc.Auth(r.Context())

	if err != nil {
		if errors.Is(err, errdomain.UserDoesNotExistError) {
			json.SendHttpResponse(w, http.StatusBadRequest, err, nil)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.SendHttpResponse(w, http.StatusOK, response, nil)
}
