package repository

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/database"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
)

type AuthRepository interface {
	GetUserByCredentials(ctx context.Context, request *domain.SignInRequest) (*domain.User, error)
	CheckUserUniqueness(ctx context.Context, user *domain.CheckUserRequest) (*domain.User, error)
	CreateUser(ctx context.Context, user *domain.SignUpRequest) (*domain.User, error)
	GetCurrentUser(ctx context.Context) (*domain.User, error)
}

type repo struct {
	logger *logger.Logger
	db     database.Driver
}

func NewAuthRepository(logger *logger.Logger, db database.Driver) AuthRepository {
	return &repo{
		logger: logger,
		db:     db,
	}
}
