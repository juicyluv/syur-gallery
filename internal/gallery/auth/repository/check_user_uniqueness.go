package repository

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (r *repo) CheckUserUniqueness(ctx context.Context, request *domain.CheckUserRequest) (*domain.User, error) {

	query := `select email, username from users where lower(username) = lower($1) or lower(email) = lower($2)`

	var user domain.User

	row := r.db.QueryRow(ctx, query, request.Username, request.Email)

	err := row.Scan(
		&user.Email,
		&user.Username,
	)

	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return &user, nil
		}

		return nil, err
	}

	return nil, err
}
