package repository

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

func (r *repo) GetUserByCredentials(ctx context.Context, request *domain.SignInRequest) (*domain.User, error) {
	//language=PostgreSQL
	query := `
select id, username, email, created_at, password_hash, is_verified, is_banned, is_trusted
from users
where email = $1 or username = $1`

	var user domain.User

	row := r.db.QueryRow(ctx, query, request.Login)

	err := row.Scan(
		&user.Id,
		&user.Username,
		&user.Email,
		&user.CreatedAt,
		&user.Password,
		&user.IsVerified,
		&user.IsBanned,
		&user.IsTrusted,
	)

	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, errdomain.ErrObjectNotFound
		}

		return nil, err
	}

	return &user, nil
}
