package repository

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (r *repo) CreateUser(ctx context.Context, user *domain.SignUpRequest) (*domain.User, error) {
	query := `INSERT INTO users(username, email, password_hash) 
	VALUES($1, $2, $3)
	RETURNING id, email, username`

	var User domain.User
	var hashedPassword string

	if hashedP, err := User.GeneratePasswordHash(user.Password); err != nil {
		return nil, err
	} else {
		hashedPassword = hashedP
	}

	err := r.db.QueryRow(
		ctx,
		query,
		user.Username,
		user.Email,
		hashedPassword,
	).Scan(&User.Id, &User.Email, &User.Username)

	return &User, err
}
