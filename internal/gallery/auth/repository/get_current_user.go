package repository

import (
	"context"
	"errors"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"

	"github.com/jackc/pgx/v5"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
)

func (r *repo) GetCurrentUser(ctx context.Context) (*domain.User, error) {
	token := ctx.Value("token").(*domain.AuthToken)

	// TODO: получать данные о бане, затем, если пользователь не заблокирован, получать теги
	query := `select email, username, created_at, last_login, is_banned, is_verified, avatar_path, description from users where id = $1`
	var user domain.User

	row := r.db.QueryRow(ctx, query, token.UserId)
	err := row.Scan(
		&user.Email,
		&user.Username,
		&user.CreatedAt,
		&user.LastLogin,
		&user.IsBanned,
		&user.IsVerified,
		&user.AvatarPath,
		&user.Description,
	)
	user.Id = token.UserId

	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, errdomain.ErrObjectNotFound
		}

		return nil, err
	}

	return &user, err
}
