package auth

import (
	"context"
	"net/http"
	"strings"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http/json"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"
)

func RequireAuth(next http.HandlerFunc, cfg config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		InvalidAuthErr := errdomain.InvalidAuthorizationHeaderError
		header := r.Header.Get("Authorization")

		if header == "" {
			json.SendHttpResponse(w, http.StatusBadRequest, InvalidAuthErr, nil)
			return
		}

		headerParts := strings.Split(header, " ")
		if len(headerParts) != 2 {
			json.SendHttpResponse(w, http.StatusBadRequest, InvalidAuthErr, nil)
			return
		}
		token, err := ParseToken(headerParts[1], cfg.SigningKey())
		if err != nil {
			json.SendHttpResponse(w, http.StatusBadRequest, err, nil)
			return
		}
		ctx := context.WithValue(r.Context(), "token", token)
		next(w, r.WithContext(ctx))
	}
}

func ParseToken(tokenString string, signingKey string) (*domain.AuthToken, error) {

	token, err := jwt.ParseWithClaims(tokenString, &domain.AuthToken{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(signingKey), nil
	})

	if err != nil {
		return nil, errdomain.UnableToParseTokenError
	}

	claims, ok := token.Claims.(*domain.AuthToken)
	if !ok || !token.Valid {
		return nil, errdomain.InvalidAuthorizationHeaderError
	}

	return claims, nil
}
