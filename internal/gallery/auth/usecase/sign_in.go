package usecase

import (
	"context"
	"errors"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
	"strings"
)

func (u *usecase) SignIn(ctx context.Context, request *domain.SignInRequest) (*domain.SignInResponse, error) {
	if request.Login == "" || request.Password == "" {
		return nil, errdomain.InvalidCredentialsError
	}

	request.Login = strings.ToLower(request.Login)

	user, err := u.repo.GetUserByCredentials(ctx, request)

	if err != nil {
		if errors.Is(err, errdomain.ErrObjectNotFound) {
			return nil, errdomain.InvalidCredentialsError
		}

		return nil, err
	}

	err = user.ComparePasswords(request.Password, user.Password)

	if err != nil {
		return nil, errdomain.WrongPasswordError
	}

	token, err := u.GenerateTokenFromUserData(user)

	if err != nil {
		return nil, err
	}

	return &domain.SignInResponse{Token: token, User: *user}, nil
}
