package usecase

import (
	"context"
	"errors"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

func (u *usecase) Auth(ctx context.Context) (*domain.AuthResponse, error) {
	user, err := u.repo.GetCurrentUser(ctx)

	if err != nil {
		if errors.Is(err, errdomain.ErrObjectNotFound) {
			return nil, errdomain.UserDoesNotExistError
		}

		return nil, err
	}
	token, err := u.GenerateTokenFromUserData(user)

	if err != nil {
		return nil, err
	}

	return &domain.AuthResponse{Token: token, User: *user}, nil
}
