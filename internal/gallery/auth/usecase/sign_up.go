package usecase

import (
	"context"
	"errors"
	"strings"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

func (u *usecase) SignUp(ctx context.Context, request *domain.SignUpRequest) (*domain.SignUpResponse, error) {
	var userRequest domain.CheckUserRequest

	userRequest.Username = request.Username
	userRequest.Email = request.Email

	if request.Email == "" || request.Username == "" || request.Password == "" {
		return nil, errdomain.EmptyCredentialsError
	}

	request.Email = strings.ToLower(request.Email)
	request.Username = strings.ToLower(request.Username)

	if user, err := u.repo.CheckUserUniqueness(ctx, &userRequest); err != nil {

		if !errors.Is(err, errdomain.ErrObjectNotFound) {

			if strings.ToLower(user.Username) == request.Username {
				return nil, errdomain.UsernameAlreadyTakenError

			} else if strings.ToLower(user.Email) == request.Email {
				return nil, errdomain.EmailAlreadyTakenError
			}
		}
		return nil, err
	}

	user, err := u.repo.CreateUser(ctx, request)

	if err != nil {
		return nil, err
	}

	token, err := u.GenerateTokenFromUserData(user)

	return &domain.SignUpResponse{Token: token, User: *user}, nil
}
