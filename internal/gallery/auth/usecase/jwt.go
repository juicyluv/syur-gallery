package usecase

import (
	"time"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain/errdomain"
)

const (
	tokenTTL = time.Hour * 24 * 2
)

func (u *usecase) GenerateTokenFromUserData(user *domain.User) (string, error) {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &domain.AuthToken{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: &jwt.NumericDate{time.Now().Add(tokenTTL)},
		},
		UserId:     user.Id,
		Email:      user.Email,
		Username:   user.Username,
		FullName:   user.FullName,
		AvatarPath: user.AvatarPath,
	})

	tokenString, err := token.SignedString([]byte(u.cfg.SigningKey()))

	if err != nil {
		return "", errdomain.FailedToSignTokenError
	}

	return tokenString, nil
}
