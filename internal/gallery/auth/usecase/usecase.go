package usecase

import (
	"context"

	"gitlab.com/juicyluv/syur-gallery/internal/gallery/auth/repository"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/domain"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
)

type AuthUseCase interface {
	SignIn(ctx context.Context, request *domain.SignInRequest) (*domain.SignInResponse, error)
	SignUp(ctx context.Context, request *domain.SignUpRequest) (*domain.SignUpResponse, error)
	GenerateTokenFromUserData(user *domain.User) (string, error)
	Auth(ctx context.Context) (*domain.AuthResponse, error)
}

type usecase struct {
	logger *logger.Logger
	repo   repository.AuthRepository
	cfg    config.Config
}

func NewAuthUseCase(logger *logger.Logger, repo repository.AuthRepository, cfg config.Config) AuthUseCase {
	return &usecase{
		logger: logger,
		repo:   repo,
		cfg:    cfg,
	}
}
