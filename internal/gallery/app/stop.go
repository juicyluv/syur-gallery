package app

import (
	"context"

	"github.com/kardianos/service"
	"go.uber.org/zap"
)

func (a *application) Stop(s service.Service) error {
	err := a.db.Close(context.Background())

	if err != nil {
		a.logger.Error("failed to close database connection", zap.Error(err))
	}

	return nil
}
