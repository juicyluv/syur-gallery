package app

import (
	stdHttp "net/http"

	"github.com/kardianos/service"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/database/postgresql"
	"go.uber.org/zap"
)

func (a *application) Start(s service.Service) error {
	dsn := a.config.DatabaseConnString()

	a.logger.Info("trying to connect to database", zap.String("DSN", dsn))

	db, err := postgresql.NewPostgreSQLConnection(dsn)

	if err != nil {
		a.logger.Error("cannot connect to database", zap.Error(err))
		return err
	}

	a.logger.Info("successfully connected to database")

	a.db = db

	a.server = http.NewServer(a.logger, a.config, a.db)

	err = a.server.Serve()

	if err != nil && err != stdHttp.ErrServerClosed {
		a.logger.Error("server error", zap.Error(err))
	}

	a.logger.Info("the server has been down successfully")

	return nil
}
