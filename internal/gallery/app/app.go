package app

import (
	"fmt"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/database"

	"github.com/kardianos/service"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/delivery/http"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/config/viper"
	"gitlab.com/juicyluv/syur-gallery/internal/gallery/infrastructure/logger"
)

type application struct {
	service service.Service
	server  http.Server
	logger  *logger.Logger

	db     database.Driver
	config config.Config
}

func New(configPath string) (gallery.Application, error) {

	cfg, err := viper.NewConfig(configPath)

	if err != nil {
		return nil, fmt.Errorf("failed to read config %s", err.Error())
	}

	appLogger, err := logger.NewLogger()

	if err != nil {
		return nil, fmt.Errorf("failed to configure logger: %s", err.Error())
	}

	app := &application{
		logger: appLogger,
		config: cfg,
	}

	srv, err := service.New(app, &service.Config{Name: "gallery"})

	if err != nil {
		return nil, fmt.Errorf("init service: %v", err)
	}

	app.service = srv

	return app, nil
}

func (a *application) Service() service.Service {
	return a.service
}

func (a *application) GetLogger() *logger.Logger {
	return a.logger
}

func (a *application) SetLogger(l *logger.Logger) {
	a.logger = l
}
