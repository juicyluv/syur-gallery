FROM golang:1.19-alpine3.16

RUN mkdir -p /app/gallery

COPY . /app/gallery

WORKDIR /app/gallery

RUN go mod download
RUN go install github.com/swaggo/swag/cmd/swag@latest

RUN swag init

RUN go build -o gallery main.go

EXPOSE 8080

CMD ["./gallery"]
