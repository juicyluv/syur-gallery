# Backend проекта веб-студии Галерея СЮР

Перед настройкой проекта необходимо его склонировать локально:

```bash
$ git clone https://gitlab.com/juicyluv/syur-gallery
```

## Запуск проекта

Необходимо установить docker и docker-compose.

```bash
$ docker-compose up --build -d
```

После этого сервер будет доступен на порту, указанном в конфиге(по умолчанию `localhost:8080`).

На локальной машине будет запущена БД(PostgreSQL), будут использованы миграции и запущен сервер на Go.

Документация доступна на `http://localhost:8080/api/v1/docs/index.html`.

## Остановка проекта

```bash
$ docker-compose down
```